#!/bin/bash


# Generates a private public key pair which will be used by the server
# to generate/decrypt JWT


OUTPUT_DIR="/var/flattie/"
PEM_KEY_FILE="$OUTPUT_DIR/private_key.pem"
PRIV_KEY="$OUTPUT_DIR/private_key.der"
PUBLIC_KEY="$OUTPUT_DIR/public_key.der"

sudo mkdir -p "$OUTPUT_DIR"

if [[ -f "$PRIV_KEY" ]]
then
	exit 0
fi

sudo openssl genrsa -out "$PEM_KEY_FILE" 2048
sudo openssl pkcs8 -topk8 -inform PEM -outform DER -in "$PEM_KEY_FILE" -out "$PRIV_KEY" -nocrypt
sudo openssl rsa -in "$PEM_KEY_FILE" -pubout -outform DER -out "$PUBLIC_KEY"

sudo chown $USER: "$PUBLIC_KEY"
sudo chown $USER: "$PRIV_KEY"
