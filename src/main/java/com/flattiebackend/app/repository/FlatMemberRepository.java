package com.flattiebackend.app.repository;

import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface FlatMemberRepository extends JpaRepository<FlatMember, Long> {
    Optional<FlatMember> findByNameAndFlat(String name, Flat flat);

    Optional<FlatMember> findByEmail(String email);

    List<FlatMember> findByFlat(Flat flat);

    Long countByFlat(Flat flat);
}
