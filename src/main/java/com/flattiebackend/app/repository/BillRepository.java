package com.flattiebackend.app.repository;

import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.FlatMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.time.Instant;
import java.util.List;

@RepositoryRestResource
public interface BillRepository extends JpaRepository<Bill, Long> {
    List<Bill> findByOriginalPayerIs(FlatMember payer);
}
