package com.flattiebackend.app.repository;

import com.flattiebackend.app.model.Flat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface FlatRepository extends JpaRepository<Flat, Long> {
    Optional<Flat> findByAddress(String address);
}
