package com.flattiebackend.app.json.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddMultiplePaymentRequest {
    private Long userId;
    private Long originalPayerId;
}
