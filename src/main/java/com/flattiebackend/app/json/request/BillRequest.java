package com.flattiebackend.app.json.request;

import lombok.*;
import org.joda.money.Money;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BillRequest {
    private Long id;
    @NotNull
    private Money amount;
    @NotBlank
    private String name;
    @NotNull
    private Instant date;
    @NotNull
    private Long payerId;
    @Builder.Default
    @NotEmpty
    private List<Long> intendedPayerIds = new ArrayList<>();
}
