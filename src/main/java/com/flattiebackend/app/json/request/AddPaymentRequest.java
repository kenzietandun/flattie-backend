package com.flattiebackend.app.json.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddPaymentRequest {
    private Long userId;
    private Long billId;
}
