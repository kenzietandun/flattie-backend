package com.flattiebackend.app.json.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class RegisterRequest {
    @NotBlank String name;
    @JsonProperty("flatCode")
    @NotNull Long flatId;
    @NotBlank String email;
    @NotBlank String password;
}