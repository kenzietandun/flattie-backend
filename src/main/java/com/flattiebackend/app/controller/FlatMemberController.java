package com.flattiebackend.app.controller;

import com.flattiebackend.app.dto.FlatMemberDTO;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.service.FlatMemberService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/flatmember")
public class FlatMemberController {
    @Autowired
    private ModelMapper mapper;

    @Autowired
    private FlatMemberService flatMemberService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<FlatMemberDTO>> readFlatMembers(@RequestParam Long memberId) {
        List<FlatMember> members = flatMemberService.readFlatMembers(memberId);
        List<FlatMemberDTO> response = members.stream()
                .map(member -> mapper.map(member, FlatMemberDTO.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<FlatMemberDTO> read(@RequestParam Long id) {
        FlatMember flatMember = flatMemberService.read(id);
        FlatMemberDTO response = mapper.map(flatMember, FlatMemberDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        flatMemberService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}