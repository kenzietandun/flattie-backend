package com.flattiebackend.app.controller;

import com.flattiebackend.app.dto.FlatMemberDTO;
import com.flattiebackend.app.dto.ObjectIdDTO;
import com.flattiebackend.app.exception.InvalidCredentialsException;
import com.flattiebackend.app.json.request.LoginRequest;
import com.flattiebackend.app.json.request.RegisterRequest;
import com.flattiebackend.app.jwt.JwtTokenUtil;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.service.FlatMemberService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class AuthenticationController {
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private FlatMemberService flatMemberService;

    @Autowired
    private ModelMapper mapper;

    @PostMapping(value = "/logout")
    public ResponseEntity<Void> logout() {
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/auth")
    public ResponseEntity<FlatMemberDTO> checkAuth() {
        FlatMember requestingFlatMember = jwtTokenUtil.getRequestingFlatMember();
        FlatMemberDTO response = mapper.map(requestingFlatMember, FlatMemberDTO.class);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<FlatMemberDTO> login(@Valid @RequestBody LoginRequest request,
                                               HttpServletResponse httpServletResponse) {
        FlatMember flatMember = flatMemberService.verify(request.getEmail(), request.getPassword())
                .orElseThrow(InvalidCredentialsException::new);

        Cookie cookie = new Cookie("jwt", jwtTokenUtil.generateToken(flatMember));
        httpServletResponse.addCookie(cookie);

        FlatMemberDTO response = mapper.map(flatMember, FlatMemberDTO.class);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping(value = "/register")
    ResponseEntity<ObjectIdDTO> register(@Valid @RequestBody RegisterRequest request) {
        FlatMember flatMember = mapper.map(request, FlatMember.class);
        Long id = flatMemberService.create(request.getFlatId(), flatMember);
        return ResponseEntity.ok(ObjectIdDTO.builder().id(id).build());
    }
}
