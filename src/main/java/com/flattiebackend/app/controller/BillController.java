package com.flattiebackend.app.controller;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.ObjectIdDTO;
import com.flattiebackend.app.dto.OwingUserDTO;
import com.flattiebackend.app.json.request.AddMultiplePaymentRequest;
import com.flattiebackend.app.json.request.BillRequest;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.service.BillService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/bills")
public class BillController {
    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BillService billService;

    @PostMapping
    public ResponseEntity<ObjectIdDTO> createBill(@Valid @RequestBody BillRequest request) {
        Bill bill = mapper.map(request, Bill.class);

        Long id = billService.create(request.getPayerId(), bill, request.getIntendedPayerIds()).getId();
        ObjectIdDTO response = ObjectIdDTO.builder().id(id).build();
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping
    public ResponseEntity<Object> readBills(@RequestParam(required = false) Long billId) {
        // User requests to read a single bill
        if (null != billId) {
            Bill bill = billService.read(billId);

            BillDTO response = mapper.map(bill, BillDTO.class);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }

        // No bill id specified, user requests to read all bills
        List<Bill> bills = billService.readAll();
        List<BillDTO> response = bills.stream().map(bill -> mapper.map(bill, BillDTO.class)).collect(
                Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(value = "/flat-member")
    public ResponseEntity<Object> readBillsByFlatMember(@RequestParam Long memberId) {
        List<Bill> bills = billService.readAllByFlatMemberId(memberId);
        List<BillDTO> response = bills.stream().map(bill -> mapper.map(bill, BillDTO.class)).collect(
                Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> updateBill(@PathVariable Long id, @RequestBody BillDTO billDTO) {
        Bill bill = mapper.map(billDTO, Bill.class);

        billService.update(id, bill);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteBill(@PathVariable Long id) {
        billService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/owing")
    public ResponseEntity<OwingUserDTO> getOwingStatus(@RequestParam Long userId) {
        OwingUserDTO owingStatusOfUser = billService.getOwingStatusOfUser(userId);
        return ResponseEntity.ok().body(owingStatusOfUser);
    }

    @PostMapping(value = "/add-payment-all")
    public ResponseEntity<List<BillDTO>> addPaymentAllBill(@RequestBody AddMultiplePaymentRequest request) {
        billService.addMultiplePayment(request.getOriginalPayerId(), request.getUserId());
        return ResponseEntity.ok().build();
    }
}