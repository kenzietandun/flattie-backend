package com.flattiebackend.app.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OwingUserDTO {
    private List<OwingDTO> owing;
    private String owingTotal;
}
