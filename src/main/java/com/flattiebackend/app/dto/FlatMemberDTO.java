package com.flattiebackend.app.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FlatMemberDTO {
    private Long id;
    private String name;
}
