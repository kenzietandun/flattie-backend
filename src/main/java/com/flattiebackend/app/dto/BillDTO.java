package com.flattiebackend.app.dto;

import lombok.*;
import org.joda.money.Money;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BillDTO {
    private Long id;
    private Money amount;
    private String name;
    private Instant date;
    private boolean paid;
    private Long originalPayerId;
    @Builder.Default
    private List<Long> payerId = new ArrayList<>();
    @Builder.Default
    private List<Long> intendedPayerId = new ArrayList<>();
    @Builder.Default
    private boolean splitEven = true;
}
