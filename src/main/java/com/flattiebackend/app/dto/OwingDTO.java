package com.flattiebackend.app.dto;

import lombok.*;
import org.joda.money.Money;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class OwingDTO {
    private Long payerId;
    private String payer;
    private Money summary;
    // Bills the person paid,and needs to be split and paid by
    private List<BillDTO> bills;
}
