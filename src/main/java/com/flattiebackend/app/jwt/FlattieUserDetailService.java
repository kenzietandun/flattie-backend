package com.flattiebackend.app.jwt;

import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.FlatMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * CustomUserDetailService
 */
@Service
public class FlattieUserDetailService implements UserDetailsService {
    @Autowired
    private FlatMemberRepository flatMemberRepository;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        final FlatMember flatMember =
                flatMemberRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException(
                        username));

        return User.withUsername(flatMember.getEmail()).password(flatMember.getPassword()).authorities(
                "ROLE_USER").build();
    }
}
