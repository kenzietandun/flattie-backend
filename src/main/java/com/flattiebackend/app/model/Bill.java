package com.flattiebackend.app.model;

import lombok.*;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    @Getter(value = AccessLevel.NONE)
    @Setter(value = AccessLevel.NONE)
    private BigDecimal amount;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private Instant date;

    @ManyToOne
    @JoinColumn(name = "original_payer_flat_member_id")
    @EqualsAndHashCode.Include
    private FlatMember originalPayer;

    @Builder.Default
    @ManyToMany(mappedBy = "allBills")
    @EqualsAndHashCode.Exclude
    private List<FlatMember> payers = new ArrayList<>();

    @Builder.Default
    @ManyToMany(mappedBy = "appointedBills")
    @EqualsAndHashCode.Exclude
    private List<FlatMember> intendedPayers = new ArrayList<>();

    public Money getAmount() {
        return Money.of(CurrencyUnit.of("NZD"), this.amount);
    }

    public void setAmount(Money amount) {
        this.amount = amount.getAmount();
    }

    @PreRemove
    private void removeFlatMemberFromBill() {
        for (FlatMember flatMember : payers) {
            flatMember.getAllBills().remove(this);
        }
        for (FlatMember flatMember : intendedPayers) {
            flatMember.getAppointedBills().remove(this);
        }
    }
}