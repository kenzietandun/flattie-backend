package com.flattiebackend.app.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {
        "name",
        "flat_id"
}))
public class FlatMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    @OneToMany(mappedBy = "originalPayer")
    @EqualsAndHashCode.Exclude
    @Builder.Default
    // bills that this flat member is the original payer of
    private List<Bill> billsFlatMemberPaid = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "flatmember_bills_paid", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "bill_id"))
    @EqualsAndHashCode.Exclude
    @Builder.Default
    // all the bills that this flat member has paid
    private List<Bill> allBills = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "flatmember_appointed_bills", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "bill_id"))
    @EqualsAndHashCode.Exclude
    @Builder.Default
    // all the bills that this flat member needs to pay
    private List<Bill> appointedBills = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "flat_id")
    @EqualsAndHashCode.Include
    private Flat flat;

    @EqualsAndHashCode.Include
    private String email;

    @EqualsAndHashCode.Include
    private String password;

    public void addBillToAppointedBills(Bill bill) {
        if (appointedBills.contains(bill)) {
            return;
        }

        bill.getIntendedPayers().add(this);
        appointedBills.add(bill);
    }

    public void removeBillFromAppointedBills(Bill bill) {
        bill.getIntendedPayers().remove(this);
        appointedBills.remove(bill);
    }

    // Add new bill to FlatMember.AllBills if it does not exist in the array
    public void addBillToAllBills(Bill bill) {
        if (allBills.contains(bill)) {
            return;
        }

        this.allBills.add(bill);
        bill.getPayers().add(this);
    }

    // Add new bill to list of bills you have paid
    public void addBillToYourBillsYouPaid(Bill bill) {
        if (billsFlatMemberPaid.contains(bill)) {
            return;
        }

        this.billsFlatMemberPaid.add(bill);
        bill.setOriginalPayer(this);
    }

    public void removeBill(Bill bill) {
        bill.getPayers().remove(this);
        this.allBills.remove(bill);
    }
}