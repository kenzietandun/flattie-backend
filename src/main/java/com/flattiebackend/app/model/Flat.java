package com.flattiebackend.app.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
public class Flat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private String address;

    @OneToMany(mappedBy = "flat")
    @EqualsAndHashCode.Exclude
    @Builder.Default
    private List<FlatMember> flatMembers = new ArrayList<>();

    public void addMember(FlatMember member) {
        flatMembers.add(member);
        member.setFlat(this);
    }

    public void removeMember(FlatMember member) {
        member.setFlat(null);
        flatMembers.remove(member);
    }
}
