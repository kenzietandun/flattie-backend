package com.flattiebackend.app.service;

import java.security.PrivateKey;
import java.security.PublicKey;

public interface KeyPairService {
    /**
     * Attempt to read existing public key from persistent storage.
     *
     * @return Found public key
     */
    PublicKey readPublicKey();

    /**
     * Attempt to read existing private key from persistent storage.
     *
     * @return Found private key
     */
    PrivateKey readPrivateKey();
}