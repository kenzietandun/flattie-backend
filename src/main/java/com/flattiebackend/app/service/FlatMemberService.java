package com.flattiebackend.app.service;

import com.flattiebackend.app.model.FlatMember;

import java.util.List;
import java.util.Optional;

public interface FlatMemberService {
    Long create(Long flatId, FlatMember flatMember);

    Optional<FlatMember> verify(String email, String password);

    FlatMember read(Long id);

    List<FlatMember> readFlatMembers(Long requesterId);

    FlatMember readByEmail(String email);

    void delete(Long id);
}
