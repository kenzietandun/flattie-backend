package com.flattiebackend.app.service;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingUserDTO;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.FlatMember;

import java.util.List;

public interface BillService {
    /**
     * Creates a bill that is split even across all members
     *
     * @param payerId Original payer's id
     * @param bill    Bill obj
     *
     * @return Saved bill obj
     */
    Bill create(Long payerId, Bill bill);

    /**
     * Creates a bill but with a selected few intended payers
     *
     * @param payerId          Original payer's id
     * @param bill             Bill obj
     * @param intendedPayerIds intended payers ids
     *
     * @return Saved bill obj
     */
    Bill create(Long payerId, Bill bill, List<Long> intendedPayerIds);

    Bill read(Long id);

    void update(Long id, Bill bill);

    void delete(Long id);

    List<Bill> readAll();

    List<Bill> readAllByFlatMemberId(Long flatMemberId);

    OwingUserDTO getOwingStatusOfUser(Long requestingMemberId);

    void addMultiplePayment(Long flatMember1Id, Long flatMember2Id);

    BillDTO toDTO(Bill bill, FlatMember requestingFlatMember);
}
