package com.flattiebackend.app.service.impl;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingDTO;
import com.flattiebackend.app.dto.OwingUserDTO;
import com.flattiebackend.app.exception.BillNotFoundException;
import com.flattiebackend.app.exception.FlatMemberNotFoundException;
import com.flattiebackend.app.exception.UnableAddPaymentNotOwingException;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.BillRepository;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.service.BillService;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class BillServiceImpl implements BillService {
    @Autowired
    BillRepository billRepository;
    @Autowired
    FlatMemberRepository flatMemberRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public Bill create(Long payerId, Bill bill) {
        FlatMember flatMember =
                flatMemberRepository.findById(payerId).orElseThrow(() -> new FlatMemberNotFoundException(
                        payerId));

        Bill savedBill = billRepository.save(bill);
        flatMember.addBillToAllBills(savedBill); // both
        flatMember.addBillToYourBillsYouPaid(savedBill);

        return savedBill;
    }

    @Override
    public Bill create(Long payerId, Bill bill, List<Long> intendedPayerIds) {
        Bill savedBill = create(payerId, bill);

        for (Long intendedPayerId : intendedPayerIds) {
            FlatMember intendedPayer = flatMemberRepository.findById(intendedPayerId)
                    .orElseThrow(() -> new FlatMemberNotFoundException(payerId));
            intendedPayer.addBillToAppointedBills(bill);
        }

        return savedBill;
    }

    @Override
    public void delete(Long id) {
        Optional<Bill> byId = billRepository.findById(id);
        if (byId.isEmpty()) {
            throw new BillNotFoundException(id);
        }

        billRepository.deleteById(id);
    }

    @Override
    public List<Bill> readAll() {
        return billRepository.findAll();
    }

    @Override
    public List<Bill> readAllByFlatMemberId(Long flatMemberId) {
        FlatMember flatMember =
                flatMemberRepository.findById(flatMemberId).orElseThrow(() -> new FlatMemberNotFoundException(
                        flatMemberId));
        return billRepository.findByOriginalPayerIs(flatMember);
    }

    @Override
    public void update(Long id, Bill newBill) {
        Bill bill = billRepository.findById(id).orElseThrow(() -> new BillNotFoundException(id));
        mapper.map(newBill, bill);
        bill.setId(id);
    }

    @Override
    public Bill read(Long id) {
        return billRepository.findById(id).orElseThrow(() -> new BillNotFoundException(id));
    }

    @Override
    public OwingUserDTO getOwingStatusOfUser(Long requestingMemberId) {
        FlatMember requestingFlatMember = flatMemberRepository.findById(requestingMemberId)
                .orElseThrow(() -> new FlatMemberNotFoundException(requestingMemberId));
        List<FlatMember> allFlatMembers = flatMemberRepository.findByFlat(requestingFlatMember.getFlat());


        List<OwingDTO> owingDTOs = generateOwingDTO(requestingFlatMember, allFlatMembers);
        String owingTotal = owingDTOs.stream().map(OwingDTO::getSummary).reduce(Money.zero(CurrencyUnit.of(
                "NZD")), Money::plus).toString();

        return OwingUserDTO.builder().owing(owingDTOs).owingTotal(owingTotal).build();
    }

    private List<OwingDTO> generateOwingDTO(FlatMember requestingFlatMember,
                                            List<FlatMember> allFlatMembers) {
        long flatMembersCount = allFlatMembers.size();
        List<OwingDTO> owingDTOs = new ArrayList<>();

        for (FlatMember flatMember : allFlatMembers) {
            if (flatMember.equals(requestingFlatMember)) {
                continue; // Requesting member do not owe themselves anything, therefore can skip it
            }

            List<Bill> requestingMemberOwingBills = getOwingBillsForFlatMember(requestingFlatMember,
                                                                               flatMember);
            Money requestingMemberOwingAmount = getOwingAmount(flatMembersCount, requestingMemberOwingBills);

            List<Bill> flatMemberOwingBills = getOwingBillsForFlatMember(flatMember, requestingFlatMember);
            Money flatMemberOwingAmount = getOwingAmount(flatMembersCount, flatMemberOwingBills);

            // Requesting member owes this flat member this much
            Money owingSummaryForFlatMember = requestingMemberOwingAmount.minus(flatMemberOwingAmount);

            owingDTOs.add(OwingDTO.builder().payer(flatMember.getName()).payerId(flatMember.getId()).bills(
                    requestingMemberOwingBills.stream()
                            .map(bill -> toDTO(bill, requestingFlatMember))
                            .collect(Collectors.toList())).summary(owingSummaryForFlatMember).build());
        }

        return owingDTOs;
    }

    /**
     * Given a list of bills, calculate the amount of money someone owe
     *
     * @param flatMembersCount           Number of flat members in the flat
     * @param requestingMemberOwingBills List of bills to be considered
     *
     * @return Amount of money someone owe
     */
    private Money getOwingAmount(long flatMembersCount, List<Bill> requestingMemberOwingBills) {
        Money owingAmount = Money.zero(CurrencyUnit.of("NZD"));
        RoundingMode defaultRoundingMode = RoundingMode.HALF_UP;

        for (Bill requestingMemberOwingBill : requestingMemberOwingBills) {
            boolean splitEven = requestingMemberOwingBill.getIntendedPayers().isEmpty();
            Money originalAmount = requestingMemberOwingBill.getAmount();


            if (splitEven) {
                owingAmount = owingAmount.plus(originalAmount.dividedBy(flatMembersCount,
                                                                        defaultRoundingMode));
            } else {
                long numPayers = requestingMemberOwingBill.getIntendedPayers().size();
                owingAmount = owingAmount.plus(requestingMemberOwingBill.getAmount()
                                                       .dividedBy(numPayers, defaultRoundingMode));
            }
        }
        return owingAmount;
    }

    /**
     * Finds all the bills that are originally paid by "Flat Member 2" and
     * has not been paid
     * by "Flat Member 1"
     *
     * @param flatMember1 Flat Member 1
     * @param flatMember2 Flat Member 2
     *
     * @return Bills not paid by "Flat Member 1"
     */
    private List<Bill> getOwingBillsForFlatMember(FlatMember flatMember1, FlatMember flatMember2) {
        List<Bill> byFlatMember = billRepository.findByOriginalPayerIs(flatMember2);

        return byFlatMember.stream()
                .filter(b -> b.getIntendedPayers().isEmpty() || b.getIntendedPayers()
                        .contains(flatMember1))
                .filter(b -> !b.getPayers().contains(flatMember1))
                .collect(Collectors.toList());
    }

    /**
     * Add payment from "Flat Member 2" to "Flat Member 1" only if "Flat
     * Member 2" owes "Flat Member 1".
     * This will Zero out all the owing status for both members
     * <p>
     * i.e.
     * Flat member 1 now does not owe anything to flat member 2
     * and flat member 2 now does not owe anything to flat member 1
     *
     * @param flatMember1Id
     * @param flatMember2Id
     */
    @Override
    public void addMultiplePayment(Long flatMember1Id, Long flatMember2Id) {
        FlatMember requestingFlatMember = flatMemberRepository.findById(flatMember2Id)
                .orElseThrow(() -> new FlatMemberNotFoundException(flatMember2Id));

        FlatMember flatMemberOwedTo = flatMemberRepository.findById(flatMember1Id)
                .orElseThrow(() -> new FlatMemberNotFoundException(flatMember1Id));

        List<OwingDTO> owingDTOs = generateOwingDTO(requestingFlatMember,
                                                    List.of(flatMemberOwedTo, requestingFlatMember));

        boolean isOwing =
                owingDTOs.stream().map(OwingDTO::getSummary).reduce(Money.zero(CurrencyUnit.of("NZD")),
                                                                    Money::plus).isPositive();

        // Only proceed if the requesting member is actually owing
        if (!isOwing) {
            throw new UnableAddPaymentNotOwingException();
        }

        // Zero out all the bills
        for (Bill bill : flatMemberOwedTo.getAllBills()) {
            requestingFlatMember.addBillToAllBills(bill);
        }

        // Zero out all the bills
        for (Bill bill : requestingFlatMember.getBillsFlatMemberPaid()) {
            flatMemberOwedTo.addBillToAllBills(bill);
        }
    }

    @Override
    public BillDTO toDTO(Bill bill, FlatMember requestingFlatMember) {
        BillDTO dto = mapper.map(bill, BillDTO.class);
        List<Long> payerIds = bill.getPayers().stream().map(FlatMember::getId).collect(Collectors.toList());
        dto.setPayerId(payerIds);
        dto.setPaid(payerIds.contains(requestingFlatMember.getId()));
        return dto;
    }
}