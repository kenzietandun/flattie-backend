package com.flattiebackend.app.service.impl;

import com.flattiebackend.app.exception.EmailAlreadyRegisteredException;
import com.flattiebackend.app.exception.FlatMemberNotFoundException;
import com.flattiebackend.app.exception.FlatNotFoundException;
import com.flattiebackend.app.exception.InvalidCredentialsException;
import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.repository.FlatRepository;
import com.flattiebackend.app.service.FlatMemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Transactional
@Service
public class FlatMemberServiceImpl implements FlatMemberService {
    @Autowired
    FlatMemberRepository flatMemberRepository;

    @Autowired
    FlatRepository flatRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Long create(Long flatId, FlatMember flatMember) {
        Flat flat = flatRepository.findById(flatId).orElseThrow(() -> new FlatNotFoundException(flatId));
        if (flatMemberRepository.findByEmail(flatMember.getEmail()).isPresent()) {
            throw new EmailAlreadyRegisteredException(flatMember.getEmail());
        }

        flatMember.setId(null);
        flatMember.setPassword(passwordEncoder.encode(flatMember.getPassword()));
        FlatMember saved = flatMemberRepository.save(flatMember);

        flat.addMember(flatMember);
        flatRepository.save(flat);

        return saved.getId();
    }

    @Override
    public Optional<FlatMember> verify(String email, String password) {
        FlatMember flatMember = flatMemberRepository.findByEmail(email).orElseThrow(
                InvalidCredentialsException::new);

        if (passwordEncoder.matches(password, flatMember.getPassword())) {
            return Optional.of(flatMember);
        }

        return Optional.empty();
    }

    @Override
    public FlatMember read(Long id) {
        FlatMember flatMember =
                flatMemberRepository.findById(id).orElseThrow(() -> new FlatMemberNotFoundException(id));
        flatMember.getAllBills();
        flatMember.getBillsFlatMemberPaid();
        return flatMember;
    }

    @Override
    public List<FlatMember> readFlatMembers(Long requesterId) {
        FlatMember requester = flatMemberRepository.findById(requesterId)
                        .orElseThrow(() -> new FlatMemberNotFoundException(requesterId));

        return requester.getFlat().getFlatMembers();
    }

    @Override
    public FlatMember readByEmail(String email) {
        return flatMemberRepository.findByEmail(email)
                .orElseThrow(() -> new FlatMemberNotFoundException(email));
    }

    @Override
    public void delete(Long id) {
        flatMemberRepository.deleteById(id);
    }
}
