package com.flattiebackend.app.service.impl;

import com.flattiebackend.app.service.KeyPairService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Service
public class KeyPairServiceImpl implements KeyPairService {
    @Value("${jwt.key.public}")
    private String publicKeyPath;
    @Value("${jwt.key.private}")
    private String privateKeyPath;

    @Override
    @Cacheable("publickey")
    public PublicKey readPublicKey() {
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(readFile(publicKeyPath));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            return null;
        }
    }

    @Override
    @Cacheable("privatekey")
    public PrivateKey readPrivateKey() {
        try {
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(readFile(privateKeyPath));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(spec);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            return null;
        }
    }

    /**
     * Reads the content of a file
     */
    private byte[] readFile(String path)
            throws IOException {
        return Files.readAllBytes(Paths.get(path));
    }
}
