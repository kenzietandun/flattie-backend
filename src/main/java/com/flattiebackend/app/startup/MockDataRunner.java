package com.flattiebackend.app.startup;

import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.repository.FlatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MockDataRunner implements CommandLineRunner {
    @Autowired
    FlatRepository flatRepository;

    private Flat flat;

    private void createMockFlat() {
        Optional<Flat> byAddress = flatRepository.findByAddress("My address");
        if (byAddress.isPresent()) {
            flat = byAddress.get();
        } else {
            flat = flatRepository.save(Flat.builder().address("My address").build());
        }

        Optional<Flat> testFlat = flatRepository.findByAddress("Testing Flat");
        if (testFlat.isPresent()) {
            flat = testFlat.get();
        } else {
            flat = flatRepository.save(Flat.builder().address("Testing Flat").build());
        }
    }

    @Override
    public void run(String... args) {
        createMockFlat();
    }
}
