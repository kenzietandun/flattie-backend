package com.flattiebackend.app;

import com.flattiebackend.app.service.BillService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public BillService billService() {
        return Mockito.mock(BillService.class);
    }
}
