package com.flattiebackend.app.service;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingDTO;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.BillRepository;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.repository.FlatRepository;
import org.joda.money.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BillServiceSimpleBillTest {
    @Autowired
    FlatMemberRepository flatMemberRepository;
    @Autowired
    FlatRepository flatRepository;
    @Autowired
    BillRepository billRepository;
    @Autowired
    FlatMemberService flatMemberService;
    @Autowired
    BillService billService;
    @Autowired
    ModelMapper mapper;

    FlatMember mayuko;
    FlatMember kenzie;
    FlatMember ryo;
    FlatMember hailee;
    Flat flat;

    @BeforeEach
    void setup() {
        billRepository.deleteAll();
        flatMemberRepository.deleteAll();
        flatRepository.deleteAll();

        flat = flatRepository.save(Flat.builder().address("Home").build());

        mayuko = FlatMember.builder().name("Mayuko").password("pass").email("1").build();
        kenzie = FlatMember.builder().name("Kenzie").password("pass").email("2").build();
        ryo = FlatMember.builder().name("Ryo").password("pass").email("3").build();
        hailee = FlatMember.builder().name("Hailee").password("pass").email("4").build();

        flatMemberService.create(flat.getId(), mayuko);
        flatMemberService.create(flat.getId(), kenzie);
        flatMemberService.create(flat.getId(), ryo);
        flatMemberService.create(flat.getId(), hailee);

        List<BillDTO> billDTOS = new java.util.ArrayList<>();
        billDTOS.add(BillDTO.builder()
                             .name("Electricity bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 4"))
                             .payerId(List.of(mayuko.getId()))
                             .build());
        billDTOS.add(BillDTO.builder()
                             .name("Gas bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 8"))
                             .payerId(List.of(kenzie.getId()))
                             .build());
        billDTOS.add(BillDTO.builder()
                             .name("Internet bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 12"))
                             .payerId(List.of(ryo.getId()))
                             .build());
        billDTOS.add(BillDTO.builder()
                             .name("Water bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 16"))
                             .payerId(List.of(hailee.getId()))
                             .build());

        for (BillDTO billDTO : billDTOS) {
            Bill bill = mapper.map(billDTO, Bill.class);
            billService.create(billDTO.getPayerId().get(0), bill);
        }
    }

    @Test
    void calculateMayukoOwing_ShouldContainCorrectOwing() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(mayuko.getId()).getOwing();

        for (OwingDTO dto : owing) {
            switch (dto.getPayer()) {
                case "Mayuko":
                    Assertions.fail();
                    break;
                case "Kenzie":
                    Assertions.assertEquals(Money.parse("NZD 1"), dto.getSummary());
                    break;
                case "Ryo":
                    Assertions.assertEquals(Money.parse("NZD 2"), dto.getSummary());
                    break;
                case "Hailee":
                    Assertions.assertEquals(Money.parse("NZD 3"), dto.getSummary());
                    break;
            }
        }
    }

    @Test
    void calculateKenzieOwing_ShouldContainCorrectOwing() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(kenzie.getId()).getOwing();

        for (OwingDTO dto : owing) {
            switch (dto.getPayer()) {
                case "Mayuko":
                    Assertions.assertEquals(Money.parse("NZD -1"), dto.getSummary());
                    break;
                case "Kenzie":
                    Assertions.fail();
                    break;
                case "Ryo":
                    Assertions.assertEquals(Money.parse("NZD 1"), dto.getSummary());
                    break;
                case "Hailee":
                    Assertions.assertEquals(Money.parse("NZD 2"), dto.getSummary());
                    break;
            }
        }
    }

    @Test
    void calculateRyoOwing_ShouldContainCorrectOwing() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(ryo.getId()).getOwing();

        for (OwingDTO dto : owing) {
            switch (dto.getPayer()) {
                case "Mayuko":
                    Assertions.assertEquals(Money.parse("NZD -2"), dto.getSummary());
                    break;
                case "Kenzie":
                    Assertions.assertEquals(Money.parse("NZD -1"), dto.getSummary());
                    break;
                case "Ryo":
                    Assertions.fail();
                    break;
                case "Hailee":
                    Assertions.assertEquals(Money.parse("NZD 1"), dto.getSummary());
                    break;
            }
        }
    }

    @Test
    void calculateHaileeOwing_ShouldContainCorrectOwing() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(hailee.getId()).getOwing();

        for (OwingDTO dto : owing) {
            switch (dto.getPayer()) {
                case "Mayuko":
                    Assertions.assertEquals(Money.parse("NZD -3"), dto.getSummary());
                    break;
                case "Kenzie":
                    Assertions.assertEquals(Money.parse("NZD -2"), dto.getSummary());
                    break;
                case "Ryo":
                    Assertions.assertEquals(Money.parse("NZD -1"), dto.getSummary());
                    break;
                case "Hailee":
                    Assertions.fail();
                    break;
            }
        }
    }
}
