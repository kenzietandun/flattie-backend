package com.flattiebackend.app.service;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingDTO;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.BillRepository;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.repository.FlatRepository;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BillServiceComplexBillsTest {
    @Autowired
    FlatMemberRepository flatMemberRepository;
    @Autowired
    FlatRepository flatRepository;
    @Autowired
    BillRepository billRepository;
    @Autowired
    BillService billService;
    @Autowired
    FlatMemberService flatMemberService;
    @Autowired
    ModelMapper mapper;

    Flat flat;
    FlatMember bobbie;
    FlatMember jacinda;

    @BeforeEach
    void setup() {
        billRepository.deleteAll();
        flatMemberRepository.deleteAll();
        flatRepository.deleteAll();

        flat = flatRepository.save(Flat.builder().address("Home").build());

        bobbie = FlatMember.builder().name("Bobbie").password("pass").email("1").build();
        jacinda = FlatMember.builder().name("Jacinda").password("pass").email("2").build();

        flatMemberService.create(flat.getId(), bobbie);
        flatMemberService.create(flat.getId(), jacinda);

        List<BillDTO> billDTOS = new java.util.ArrayList<>();
        billDTOS.add(BillDTO.builder()
                             .name("Electricity bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 4"))
                             .payerId(List.of(bobbie.getId()))
                             .build());
        billDTOS.add(BillDTO.builder()
                             .name("Internet bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 4"))
                             .payerId(List.of(bobbie.getId()))
                             .build());
        billDTOS.add(BillDTO.builder().name("Maintenance bill").date(Instant.now()).amount(Money.parse(
                "NZD 28")).payerId(List.of(jacinda.getId())).build());
        billDTOS.add(BillDTO.builder()
                             .name("Water bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 8"))
                             .payerId(List.of(jacinda.getId()))
                             .build());
        for (BillDTO billDTO : billDTOS) {
            Bill bill = mapper.map(billDTO, Bill.class);
            billService.create(billDTO.getPayerId().get(0), bill);
        }
    }

    @Test
    void bobbieShouldHaveCorrectOwing() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(bobbie.getId()).getOwing();

        List<String> payerNames =
                owing.stream().map(OwingDTO::getPayer).collect(Collectors.toUnmodifiableList());
        Assertions.assertTrue(payerNames.contains("Jacinda"));
    }

    @Test
    void bobbieShouldHaveTwoOwingBills() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(bobbie.getId()).getOwing();

        List<BillDTO> bills = owing.stream().map(OwingDTO::getBills).flatMap(Collection::stream).collect(
                Collectors.toUnmodifiableList());

        Assertions.assertEquals(2, bills.size());
    }

    @Test
    void bobbieShouldOweCorrectAmount() {
        List<OwingDTO> owing = billService.getOwingStatusOfUser(bobbie.getId()).getOwing();

        Money owingAmount =
                owing.stream().map(OwingDTO::getSummary).reduce(Money.zero(CurrencyUnit.of("NZD")),
                                                                Money::plus);
        Assertions.assertEquals(Money.parse("NZD 14.00"), owingAmount);
    }
}