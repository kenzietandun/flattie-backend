package com.flattiebackend.app.service;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingDTO;
import com.flattiebackend.app.dto.OwingUserDTO;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.BillRepository;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.repository.FlatRepository;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BillServiceSelectedPayeeTest {
    @Autowired
    FlatMemberRepository flatMemberRepository;
    @Autowired
    FlatRepository flatRepository;
    @Autowired
    BillRepository billRepository;
    @Autowired
    BillService billService;
    @Autowired
    FlatMemberService flatMemberService;
    @Autowired
    ModelMapper mapper;

    Flat flat;
    FlatMember alice;
    FlatMember bob;
    FlatMember charlie;
    FlatMember dimitri;

    @BeforeEach
    void setup() {
        billRepository.deleteAll();
        flatMemberRepository.deleteAll();
        flatRepository.deleteAll();

        flat = flatRepository.save(Flat.builder().address("Home").build());

        alice = FlatMember.builder().name("alice").password("password").email("1").build();
        bob = FlatMember.builder().name("bob").password("password").email("2").build();
        charlie = FlatMember.builder().name("charlie").password("password").email("3").build();
        dimitri = FlatMember.builder().name("dimitri").password("password").email("4").build();

        flatMemberService.create(flat.getId(), alice);
        flatMemberService.create(flat.getId(), bob);
        flatMemberService.create(flat.getId(), charlie);
        flatMemberService.create(flat.getId(), dimitri);

        BillDTO billDTO = BillDTO.builder().name("Electricity bill").date(Instant.now()).amount(Money.parse(
                "NZD 30")).originalPayerId(alice.getId()).intendedPayerId(List.of(alice.getId(), bob.getId(),
                                                                                  charlie.getId())).build();

        Bill bill = mapper.map(billDTO, Bill.class);
        billService.create(billDTO.getOriginalPayerId(), bill, billDTO.getIntendedPayerId());
    }

    @Test
    void dimitriShouldHaveNoOwing() {
        OwingUserDTO dimitriOwingStatus = billService.getOwingStatusOfUser(dimitri.getId());

        Money total = dimitriOwingStatus.getOwing().stream().map(OwingDTO::getSummary).reduce(Money.zero(
                CurrencyUnit.of("NZD")), Money::plus);

        Assertions.assertTrue(total.isZero(), "dimitri is not in the intended payee list");
    }

    @Test
    void bobAndCharlieShouldHaveOwingAmount() {
        for (Long memberId : List.of(bob.getId(), charlie.getId())) {
            OwingUserDTO owingStatus = billService.getOwingStatusOfUser(memberId);

            Money total = owingStatus.getOwing().stream().map(OwingDTO::getSummary).reduce(Money.zero(
                    CurrencyUnit.of("NZD")), Money::plus);

            Assertions.assertEquals(Money.parse("NZD 10"),
                                    total,
                                    "$30 divided by 3 person should be $10 each");
        }
    }

    @Test
    void aliceShouldHaveNoOwing() {
        OwingUserDTO owingStatus = billService.getOwingStatusOfUser(alice.getId());

        Money total = owingStatus.getOwing().stream().map(OwingDTO::getSummary).reduce(Money.zero(CurrencyUnit
                                                                                                          .of("NZD")),
                                                                                       Money::plus);

        Assertions.assertTrue(total.isNegativeOrZero());
    }
}
