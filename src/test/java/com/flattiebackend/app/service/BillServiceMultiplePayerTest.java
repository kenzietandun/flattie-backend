package com.flattiebackend.app.service;

import com.flattiebackend.app.dto.BillDTO;
import com.flattiebackend.app.dto.OwingDTO;
import com.flattiebackend.app.model.Bill;
import com.flattiebackend.app.model.Flat;
import com.flattiebackend.app.model.FlatMember;
import com.flattiebackend.app.repository.BillRepository;
import com.flattiebackend.app.repository.FlatMemberRepository;
import com.flattiebackend.app.repository.FlatRepository;
import org.joda.money.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BillServiceMultiplePayerTest {
    @Autowired
    FlatMemberRepository flatMemberRepository;
    @Autowired
    FlatRepository flatRepository;
    @Autowired
    BillRepository billRepository;
    @Autowired
    BillService billService;
    @Autowired
    FlatMemberService flatMemberService;
    Flat flat;
    FlatMember bobbie;
    FlatMember jacinda;
    FlatMember kim;
    @Autowired
    private ModelMapper mapper;

    @BeforeEach
    void setup() {
        billRepository.deleteAll();
        flatMemberRepository.deleteAll();
        flatRepository.deleteAll();

        flat = flatRepository.save(Flat.builder().address("Home").build());

        bobbie = FlatMember.builder().name("Bobbie").password("pass").email("1").build();
        jacinda = FlatMember.builder().name("Jacinda").password("pass").email("2").build();
        kim = FlatMember.builder().name("Kim").password("pass").email("3").build();

        flatMemberService.create(flat.getId(), bobbie);
        flatMemberService.create(flat.getId(), jacinda);
        flatMemberService.create(flat.getId(), kim);

        List<BillDTO> billDTOS = new java.util.ArrayList<>();
        billDTOS.add(BillDTO.builder()
                             .name("Electricity bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 4"))
                             .originalPayerId(bobbie.getId())
                             .build());
        billDTOS.add(BillDTO.builder()
                             .name("Internet bill")
                             .date(Instant.now())
                             .amount(Money.parse("NZD 4"))
                             .originalPayerId(jacinda.getId())
                             .build());
        for (BillDTO billDTO : billDTOS) {
            Bill bill = mapper.map(billDTO, Bill.class);
            billService.create(billDTO.getOriginalPayerId(), bill);
        }
        billService.addMultiplePayment(bobbie.getId(), kim.getId());
        billService.addMultiplePayment(jacinda.getId(), kim.getId());
    }

    @Test
    void kimPayingAllBill_ShouldNotOweBobbieAndJacinda() {
        List<OwingDTO> kimOwing = billService.getOwingStatusOfUser(kim.getId()).getOwing();

        Assertions.assertEquals(Money.parse("NZD 0"), kimOwing.get(0).getSummary());
        Assertions.assertEquals(Money.parse("NZD 0"), kimOwing.get(1).getSummary());
    }

    @Test
    void bobbiePayingAllBill_ShouldNotOweKimAndJacinda() {
        List<OwingDTO> bobbieOwing = billService.getOwingStatusOfUser(bobbie.getId()).getOwing();

        Assertions.assertEquals(Money.parse("NZD 0"), bobbieOwing.get(0).getSummary());
        Assertions.assertEquals(Money.parse("NZD 0"), bobbieOwing.get(1).getSummary());
    }
}
