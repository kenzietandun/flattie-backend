package com.flattiebackend.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flattiebackend.app.json.request.BillRequest;
import org.joda.money.Money;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BillControllerCreateTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testCreateBillWithoutIntendedPayerIdShouldReturnBadRequest()
            throws Exception {
        BillRequest request = BillRequest.builder().amount(Money.parse("NZD 10")).date(Instant.now()).name(
                "Water bill").build();
        mockMvc.perform(post("/api/v1/bills").contentType("application/json")
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

}
